const { getXlsxStream } = require("xlstream");
const express = require("express");
const fs = require("fs");

const app = express();
const port = 3000;
const testFolder = "./uploads/";

app.get("/", (req, res) => {
  readFromUploads((err, data) => {
    if (err) {
      res.json({ msg: "disaster" });
    } else {
      res.json({ msg: "you tried", data });
    }
  });
});

readFromUploads = (result) => {
  fs.readdir(testFolder, async (err, files) => {
    const streamFiles = [];
    const data = [];
    for (const file of files) {
      let stream;
      try {
        stream = await getXlsxStream({
          filePath: testFolder + file,
          sheet: 0,
          withHeader: true,
          ignoreEmpty: true,
        });
        const array = [];
        const header = [];
        stream.on("data", async (x) => {
          stream.pause();
          array.push(x.formatted.arr);
          if (header.length === 0) {
            header.push(x.header);
          }
          stream.resume();
        });
        stream.on("error", (error) => {
          result(err, null);
        });

        streamFiles.push(
          new Promise((resolve, reject) => {
            stream.on("end", () => {
              resolve({ array, header });
            });
          })
        );
      } catch (err) {
        result(err, null);
      }
    }
    Promise.all(streamFiles).then((res) => result(null, res));
  });
};

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
