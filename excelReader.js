const { getXlsxStream } = require("xlstream");

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function importExcelFile(file) {
  let stream;
  console.log(file);
  try {
    stream = await getXlsxStream({
      filePath: file,
      sheet: 0,
      withHeader: true,
      ignoreEmpty: true,
    });
    console.log(stream);
  } catch (err) {
    console.log(err);
  }
  const array = [];
  const header = [];
  stream.on("data", async (x) => {
    stream.pause();
    array.push(x.formatted.arr);
    if (header.length === 0) {
      header.push(x.header);
    }
    // console.log(x.formatted.arr);
    // await sleep(2000);
    stream.resume();
  });
  stream.on("error", (error) => {
    console.log(error);
  });
  await new Promise(() =>
    stream.on("end", () => {
      console.log(array);
      console.log(header);
    })
  );
}

importExcelFile("portfolio.xlsx");
